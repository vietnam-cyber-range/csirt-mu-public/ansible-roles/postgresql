# Ansible role - PostgreSQL

By default this role serves just for automatic installation of PostgreSQL cluster. 

Main purpose of this role:
* Create database
* Initialize database using SQL script
* Create user
* Grant user privileges on database
* Specify authentication rules

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

* Also requires Ansible variables, therefore do not disable directive `gather_facts`.

## Role paramaters

All parameters are optional but some of them depends on others.

* `postgresql_listen_addresses` - Comma-separated list of addresses to listen on.
* `postgresql_role_name` - The name of the role.
* `postgresql_role_password` - The password of the role `postgresql_role_name`.
* `postgresql_role_encrypted` - Boolean value if password is encrypted or not (default `no`).
* `postgresql_database_name` - The name of the database.
* `postgresql_database_owner` - The owner of the database `postgresql_database_name` (default `postgres`).
* `postgresql_database_scripts` - The list of SQL scripts that will be executed on database `postgresql_database_name` right after creation.
                                  If `postgresql_role_name`, `postgresql_role_password` and `postgresql_database_owner` is defined scripts are executed by user `postgresql_role_name`.

Privileges depends on `postgresql_role_name` and `postgresql_database_name`.

* `postgresql_privileges_type` - Type of database object to set privileges on (one of: [`table`|`sequence`|`function`|`database`|`schema`|`language`|`tablespace`|`group`]).
* `postgresql_privileges_grant_option` - Whether role may grant specified privileges/group membership to others.
* `postgresql_privileges_objs` - Comma separated list of database objects to set privileges on (if type is one of: [`table`|`sequence`] you can specify special value ALL_IN_SCHEMA).
* `postgresql_privileges_privs` - Comma separated list of privileges to grant (one of: [`ALL`|`SELECT`|`INSERT`|`UPDATE`|`DELETE`|`TRUNCATE`|`REFERENCES`|`TRIGGER`|`CREATE`|`CONNECT`|`TEMPORARY`|`EXECUTE`|`USAGE`]).
* `postgresql_privileges_schema` - Schema that contains the database objects specified via obj (may only be provided if type is one of: [`table`|`sequence`|`function`]).
     
    For more information read Ansible documentation for module [postgresql_privs](http://docs.ansible.com/ansible/latest/postgresql_privs_module.html).
* `postgresql_pghba_rules` - List of host-based authentication rules. Each rule may consists of following attributes.
    * `type` (mandatory) - Rule type (one of: [`local`|`host`|`hostssl`|`hostnossl`]).
    * `database` (mandatory) - Name of the database (default: `postgresql_database.name`).
    * `user` (mandatory) - Name of the role (default: `postgresql_role.name`).
    * `address` (optional) - Specify client machine address (this field apply only to one of: [`host`|`hostssl`|`hostnossl`]).
    * `mask` (optional) - Specify client machine mask (this field apply only to one of: [`host`|`hostssl`|`hostnossl`]).
    * `method` (mandatory) - Specifies the authentication method to use when a connection matches this record (one of: [`trust`|`reject`|`scram-sha-256`|`md5`|`password`|`gss`|`sspi`|`ident`|`peer`|`ldap`|`radius`|`cert`|`pam`|`bsd`]).
    * `options` (optional) - Field of the form name=value that specify options for the authentication method.
    * `state` (optional) - Whether the rule should be there or not (one of: [`present`|`absent`]).
    
    For more information read PostgreSQL documentation for [pg_hba.conf](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html).

## Example

Example of simple PostgreSQL installation.

```yml
roles:
    - role: postgresql
      become: yes
```

PostgreSQL installation, that creates role `my_user` with database `my_db`, initializes `my_db` using SQL script `tables.sql` and allows connection from network.


```yml
roles:
    - role: postgresql
      postgresql_listen_addresses: '*' 
      postgresql_role_name: my_user
      postgresql_role_password: my_pass
      postgresql_database_name: my_db
      postgresql_database_scripts:
        - /tmp/tables.sql
      postgresql_privileges_type: table
      postgresql_privileges_privs: ALL
      postgresql_privileges_objs: ALL_IN_SCHEMA
      postgresql_pghba_rules:
          - type: host
            address: 172.16.1.0/24
            method: md5
      become: yes
```
